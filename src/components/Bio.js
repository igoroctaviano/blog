import React from 'react';
import md5 from 'md5';

// Import typefaces
import 'typeface-montserrat';
import 'typeface-merriweather';

import { rhythm } from '../utils/typography';

class Bio extends React.Component {
  render() {
    return (
      <div
        style={{
          display: 'flex',
          marginBottom: rhythm(2.5)
        }}
      >
        <img
          src={`https://www.gravatar.com/avatar/${md5('igoroctaviano@gmail.com')}`}
          alt="Igor Octaviano"
          style={{
            marginRight: rhythm(1 / 2),
            marginBottom: 0,
            width: rhythm(2),
            height: rhythm(2)
          }}
        />
        <p>
          Written by <strong>Igor Octaviano</strong>
{' '}
who lives and works in Belo Horizonte building
          useful things.
{' '}
          <a href="https://twitter.com/igoroctaviano">You should follow him on Twitter</a>
        </p>
      </div>
    );
  }
}

export default Bio;
